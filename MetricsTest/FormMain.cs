﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics.Metrics;


//Metrics API
using OpenTelemetry;
using OpenTelemetry.Metrics;
using OpenTelemetry.Trace;
using OpenTelemetry.Logs;

//Open Telemetry 
using OpenTelemetry.Exporter.OpenTelemetryProtocol;
using OpenTelemetry.Extensions.Hosting;
using OpenTelemetry.Resources;
using OpenTelemetry.Exporter;

namespace MetricsTest
{
    public partial class FormMain : Form
    {
        int counter = 0;
        Meter testExec_meter = null;
        Counter<int> testExec_pressButton = null;

        public FormMain()
        {
            InitializeComponent();


            testExec_meter = new Meter("AEF.TestExec", "1.0.0");
            //testExec_pressButton = testExec_meter.CreateCounter<int>("Pressed Button", "Count", "The number of times the button was pressed.");

            MeterProviderBuilder meterProviderBuilder = null;

            meterProviderBuilder = Sdk.CreateMeterProviderBuilder()
                    .ConfigureResource(r => r.AddService("MetricsTest"))
                    .AddMeter("AEF.TestExec");

            meterProviderBuilder
                 .AddOtlpExporter((exporterOptions, metricReaderOptions) =>
                 {
                     exporterOptions.Protocol = OtlpExportProtocol.HttpProtobuf;

                     /* Since we want to write metrics using HTTP instead of RPC, we need to have
                       * the correct endpoint of the http receiver inside the otlp collector
                       * 
                       * See the following link for details
                       * https://github.com/open-telemetry/opentelemetry-collector/blob/main/receiver/otlpreceiver/README.md#writing-with-httpjson
                       */

                    exporterOptions.Endpoint = new Uri("http://localhost:4318/v1/metrics");

                    metricReaderOptions.PeriodicExportingMetricReaderOptions.ExportIntervalMilliseconds = 2000;
                    metricReaderOptions.TemporalityPreference = MetricReaderTemporalityPreference.Cumulative;
                 });

            // Build will spawn a new worker thread OpenTelemetry-PeriodicExportingMetricReader-OtlpMetricExporter

            MeterProvider meterProvider = meterProviderBuilder.Build();

            testExec_pressButton = testExec_meter.CreateCounter<int>("Pressed Button", "Count", "The number of times the button was pressed.");

            //while(true) 
            //{
            //    counter?.Add(1);

            //    Task.Delay(500).Wait();
            //}

            meterProvider.Dispose();
            testExec_meter.Dispose();
  
        }

        private void button1_Click(object sender, EventArgs e)
        {

            

            //testExec_pressButton = testExec_meter.CreateCounter<int>("Pressed Button", "Count", "The number of times the button was pressed.");
            //Counter<int> buttonCounter = testExec_meter.CreateCounter<int>("alwdkjawldkj");

           counter++;
           textBox1.AppendText(counter.ToString() + "\r\n");
           testExec_pressButton?.Add(1);
           Task.Delay(500).Wait();
        }
    }
}

